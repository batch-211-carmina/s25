// console.log("Hello World");

//JSON Objects
/*
	- JSON stands for JavaScript Object Notation
	- JSON is also used in other programming languages
	- JavaScript objects are not to be confused with JSON
	- Serialization is the process of converting data into series of bytes for easier transmission/transfer of information
	_ Uses double quotes for property names

	SYNTAX:
		{
			"propertyA" : "valueA",
			"propertyB" : "valueB",
		}
*/


// JSON Objects
/*
	"city" : "Quezon City",
	"province" : "Metro Manila",
	"country" : "Philippines"
*/


//JSON Arrays
/*
	"cities" : [
		{ "city" : "Quezon City", "province" : "Metro Manila", "country" : "Philippines"},
		{ "city" : "Manila City", "province" : "Metro Manila", "country" : "Philippines"},
		{ "city" : "Makati City", "province" : "Metro Manila", "country" : "Philippines"}
	]
*/


// JSON Methods
/*
	- The JSON object contains methods for parsing and converting data into stringified JSON
*/


// Converting Data into Stringified JSON
/*
	- Stringified JSON is a JavaScript object converted into a string to be used in other functions of JS application
*/

	let batchesArr = [{batchName: 'BatchX'}, {batchName: 'BatchY'}];


// The "stringify" method is used to convert JavaScript objects into a string
	console.log('Result from stringify method:');
	console.log(JSON.stringify(batchesArr));

	let data = JSON.stringify({
		name : 'John',
		age : 31,
		address : {
			city : 'Manila',
			country : 'Philippines'
		}
	});

	console.log(data);


// Using Stringify Method with Variables
/*
	- When information is stored in a variable and is not hard coded into an object that is being stringified, we can supply the value with a variable
	- This is commonly used when the information to be stored and sent to a backed application will come from a frontend application

	SYNTAX:
		JSON.stringify({
			propertyA: variableA,
			propertyB: variableB
		});
*/

	// let firstName = prompt("What is your firsname?");
	// let lastName = prompt("What is your lastname?");
	// let age = prompt("What is your age?");
	// let address = {
	// 	city: prompt("Which city do you live in?"),
	// 	country: prompt("Which country does your city address belong to?")
	// };

	// let otherData = JSON.stringify({
	// 	firstName: firstName,
	// 	lastName: lastName,
	// 	age: age,
	// 	address: address
	// });
	// console.log(otherData);


// Converting stringified JSON into JavaScript objects
/*
	- Object are commond data types used in applications because of the complex data structures that can be created out of them
	- Information is commonly sent to applications in stringified JSON and then converted back into objects
	- This happens both for sending information to a backend application and vice versa
*/

	let batchesJson = `[{"batchName": "BatchX"}, {"batchName": "BatchY"}]`;

	console.log("Result from parse method:");
	console.log(JSON.parse(batchesJson));

	let stringifiedObject = `{"name": "John", "age": "31", "address": {"city": "Manila", "country": "Philippines"} }`;

	console.log(JSON.parse(stringifiedObject));



